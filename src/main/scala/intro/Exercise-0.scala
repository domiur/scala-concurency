package intro

import common.parallel
import org.scalameter.{Key, Warmer, config}

import scala.util.Random

// compose
object Exercise1 extends App{
  def compose[A,B,C](g:B=>C,f:A=>B):A=>C =  g compose f
  def f(x:Int):String = x.toString
  def g(x:String):Char = x.toCharArray.apply(0)
  println(compose(g,f)(310))
  println(g(f(310)))
}

//fuse
object Exercise2 extends App{
  def fuse[A,B](a:Option[A],b:Option[B]):Option[(A,B)] = {
    for( aa <- a; bb <- b)
      yield (aa,bb)
  }
}


object Exercise3 extends App{
  def check[T](xs:Seq[T])(pred:T=>Boolean) = {
    xs.map(x=>try{
      pred(x)
    }catch {
      case _ => false
    }
    ).reduce((x:Boolean,y:Boolean) => x && y)
  }
  println(check( 0 until 10)(40/_>0))
  println(check( 1 until 10)(40/_>0))
}

object Exercise4 extends App {
  case class Pair[A,B](first:A,second:B)
  val q:Any=Pair[Int,String](10,"q")
  val p:Any=Pair[String,Int]("a",20)
  val s:Any=10
  q match{
    case Pair(first:Int,second:String) => println(second)
    case Pair(first:String,second:Int) => println(first)
    case _ => println("none")
  }
}


object Exercise5 extends App {
  def permutation(s:String):List[String]= {
    def permute(l:List[String]):List[List[String]]= {
      def loop(ll: List[String], n: Int): List[List[String]] =
        if (ll.isEmpty || n == 0) Nil
        else ll :: loop(ll.tail ++ List(ll.head), n - 1)

      if (l.isEmpty) List(Nil)
      else loop(l, l.size).flatMap(x => permute(x.tail).map(y => x.head :: y))
    }
    permute(s.split("").toList).map(_.mkString(""))
  }
  println(permutation("123"))


  def permutation2(s:String):List[String]= {
    def permute(l:List[String]):List[List[String]]= {
      def loop(ll:List[String],n:Int,part:List[String]):List[List[String]]=n match {
        case 0 => Nil
        case 1 => for(l <- ll) yield l::part
        case _ => for(l <- ll;q <- loop(ll,n-1,l::part)) yield q
      }
      loop(l,l.size,Nil)
    }
    permute(s.split("").toList).map(_.mkString(""))
  }
  println(permutation2("123"))
}



object Exercise6 extends App{
  /* def combinations1(size:Int,xs:Seq[Int]):Iterator[Seq[Int]] = {
     def comb(size1:Int,xs1:Seq[Int]):Seq[Seq[Int]] = {
       if(size1==0 || xs1.isEmpty || size1>xs1.length) Seq(Nil)
       else if(size1==xs1.length) Seq(xs1)
       else {
         comb(size1 - 1, xs1.tail).map(x => Seq(xs1.head) ++ x) ++ comb(size1,xs1.tail)
       }
     }
     comb(size,xs).flatMap(x=> Iterator(x))
   }

   def combinations(size:Int,xs:Seq[Int]):Iterator[Seq[Int]] = {
     def comb(size1:Int,xs1:Seq[Int]):Seq[Seq[Int]] = {
       if(size1==0 || xs1.isEmpty || size1>xs1.length) Nil
       else if(size1==xs1.length) xs1
       else {
         comb(size1 - 1, xs1.tail).map(x => Seq(xs1.head) ++ x) ++ comb(size1,xs1.tail)
       }
     }
     comb(size,xs).iterator
   }

   combinations(3,List(1,2,3,4,5)) foreach println
   println
   combinations(3,Vector(1,2,3,4,5))  foreach println
   */

}




object Exercise8 extends App{
  class Person
  class Office
  class Msg
  class Boss

}



