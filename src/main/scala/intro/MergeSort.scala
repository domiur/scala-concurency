package intro
import common.parallel
import org.scalameter.{Key, Warmer, config}

import scala.util.Random

object mergesort extends App {
  def swap(ints: Array[Int], i: Int, i1: Int) = {
    val q = ints(i)
    ints(i) = ints(i1)
    ints(i1) = q
  }

  //stupid sort
  def sort0(ints: Array[Int], from: Int, until: Int): Array[Int] = {
    if (ints.isEmpty || from < 0 || until > ints.length || until - from <= 1)  ints
    else {
      var isExchangeHappen = false
      do {
        isExchangeHappen = false
        var i0 = from
        while (i0 < until - 1 && !isExchangeHappen) {
          if (ints(i0) > ints(i0 + 1)) {
            swap(ints, i0, i0 + 1)
            isExchangeHappen = true
          }
          i0 += 1
        }
      } while (isExchangeHappen)
      ints
    }
  }
  // buble sort with moving biggest up to end and reduce upper limit
  def sort1(ints: Array[Int], from: Int, until: Int): Array[Int] = {
    var stat=0
    if (ints.isEmpty || from < 0 || until > ints.length || until - from <= 1) ints
    else {
      var isExchangeHappen = false
      var upperLimit=until
      do {
        isExchangeHappen = false
        var i0 = from
        while (i0 < upperLimit - 1) {
          stat+=1
          if (ints(i0) > ints(i0 + 1)) {
            swap(ints, i0, i0 + 1)
            isExchangeHappen = true
          }
          i0 += 1
        }
        upperLimit -= 1

      } while (isExchangeHappen)
      println(s"sort1=$stat")
      ints
    }
  }
  // buble sort with moving biggest up to end + moving lowest to the home + reduce upper and down limits
  def sort2(ints: Array[Int], from: Int, until: Int): Array[Int] = {
    if (ints.isEmpty || from < 0 || until > ints.length || until - from <= 1) ints
    else {
      var stat=0
      var isExchangeHappen = false
      var upperLimit = until
      var lowerLimit = from
      var step=1

      do {
        var ii = if(step>0) lowerLimit else upperLimit-1
        isExchangeHappen = false
        while (lowerLimit <= ii && lowerLimit <= ii + step && ii < upperLimit && ii + step < upperLimit) {
          stat+=1
          if (( step>0 && ints(ii) > ints(ii + step)) || ( step<0 && ints(ii) < ints(ii + step))) {
            swap(ints, ii, ii + step)
            isExchangeHappen = true
          }
          ii += step
        }
        if(step>0) {
          upperLimit -= 1
        }
        else {
          lowerLimit += 1
        }
        step = -step
      } while (isExchangeHappen && lowerLimit < upperLimit)
      println(s"sort2=$stat")
      ints
    }
  }
  // as step2 + reduce step from big to 1
  def sort3(ints: Array[Int], from: Int, until: Int): Array[Int] = {
    if (ints.isEmpty || from < 0 || until > ints.length || until - from <= 1) ints
    else {
      var stat=0
      var step=ints.length
      do {
        var isExchangeHappen = false
        var upperLimit = until
        var lowerLimit = from
        step=(step/1.3).toInt
        do {
          var ii = if(step>0) lowerLimit else upperLimit-1
          isExchangeHappen = false
          while (lowerLimit <= ii && lowerLimit <= ii + step && ii < upperLimit && ii + step < upperLimit) {
            stat+=1
            if (( step>0 && ints(ii) > ints(ii + step)) || ( step<0 && ints(ii) < ints(ii + step))) {
              swap(ints, ii, ii + step)
              isExchangeHappen = true
            }
            ii += step
          }
          if (step==1) {
            upperLimit -= 1
          }
          else if(step == -1){
            lowerLimit += 1
          }
          step = -step
        } while (isExchangeHappen && lowerLimit < upperLimit && (step==1 ||step == -1))
      }while(step < -1 || step > 1)
      println(s"sort3=$stat")

      ints
    }
  }
  var ar = Array(5, 4, 3, -50, 2, 1)
  println(sort1(ar, 0, ar.length).toList)
  ar = Array(5, 4, 3, -50, 2, 1)
  println(sort2(ar, 0, ar.length).toList)
  ar = Array(5, 4, 3, -50, 2, 1)
  println(sort3(ar, 0, ar.length).toList)

  // merge 2 sorted arrays
  def mergeSort(in: Array[Int], out: Array[Int], from: Int, mid: Int, until: Int) = {
    var i1 = from
    var i2 = mid
    var ind = from
    while (i1 < mid && i2 < until) {
      if (in(i1) < in(i2)) {
        out(ind) = in(i1)
        i1 += 1
      }
      else {
        out(ind) = in(i2)
        i2 += 1
      }
      ind += 1
    }
    while (i1 < mid) {
      out(ind) = in(i1)
      i1 += 1
      ind += 1
    }
    while (i2 < until) {
      out(ind) = in(i2)
      i2 += 1
      ind += 1
    }
  }

  val q1 = Array(1, 3, 5, -2, 4, 6)
  val q2 = Array(0, 0, 0, 0, 0, 0)
  mergeSort(q1, q2, 0, 3, 6)
  println(q1.toList)
  println(q2.toList)

  def copy(in: Array[Int], out: Array[Int], from: Int, until: Int) = {
    var i = from
    while (i < until) {
      out(i) = in(i)
      i+=1
    }
  }

  def sortPar(ar: Array[Int]): Array[Int] = {
    val maxDepth = 20

    def sortPar(in: Array[Int], out: Array[Int], from: Int, until: Int, depth: Int): Unit = {
      if (depth >= maxDepth) {
        sort3(in, from, until)
      }
      else {
        val n: Int = (until + from) / 2
        parallel(sortPar(in, out, from, n, depth + 1), sortPar(in, out, n, until, depth + 1))
        mergeSort(in, out, from, n, until)
        copy(out,in,from, until)
      }
    }

    sortPar(ar, new Array[Int](ar.length), 0, ar.length, 0)
    ar
  }
  val w=Array(0,3,6,2,-9,1,4,5)
  println(sort3(w,0,w.length).toList)


  ar = new Array[Int](10000)
  for (i <- ar.indices) {
    ar(i) = Random.nextInt()
  }
  (sort1(ar, 0, ar.length).toList)
  for (i <- ar.indices) {
    ar(i) = Random.nextInt()
  }
  (sort2(ar, 0, ar.length).toList)
  for (i <- ar.indices) {
    ar(i) = Random.nextInt()
  }
  (sort3(ar, 0, ar.length).toList)

  if(false) {
    val ww = new Array[Int](1000000)

    val time1 = 0 /*config(
    Key.exec.minWarmupRuns -> 20,
    Key.exec.maxWarmupRuns -> 60,
    Key.exec.benchRuns -> 60,
    Key.verbose -> true
  ) withWarmer (new Warmer.Default) measure{
    for(i <- (0 until ww.length)){ ww(i)=Random.nextInt()}
    sort1(ww,0,ww.length)
  }*/
    val time2 = config(
      Key.exec.minWarmupRuns -> 20,
      Key.exec.maxWarmupRuns -> 60,
      Key.exec.benchRuns -> 60,
      Key.verbose -> true
    ) withWarmer (new Warmer.Default) measure {
      for (i <- ww.indices) {
        ww(i) = Random.nextInt()
      }
      sort2(ww, 0, ww.length)
    }

    val time3 = config(
      Key.exec.minWarmupRuns -> 20,
      Key.exec.maxWarmupRuns -> 60,
      Key.exec.benchRuns -> 60,
      Key.verbose -> true
    ) withWarmer (new Warmer.Default) measure {
      for (i <- ww.indices) {
        ww(i) = Random.nextInt()
      }
      sortPar(ww)
    }

    val time4 = config(
      Key.exec.minWarmupRuns -> 20,
      Key.exec.maxWarmupRuns -> 60,
      Key.exec.benchRuns -> 60,
      Key.verbose -> true
    ) withWarmer (new Warmer.Default) measure {
      for (i <- ww.indices) {
        ww(i) = Random.nextInt()
      }
      sort3(ww, 0, ww.length)
    }

    println(s"end=$time1 $time2 $time3 $time4")
  }
}





import org.scalameter.{Key, Warmer, _}
import common.{parallel, _}

import scala.util.Random

object MergeSort extends App {
  private def sort1 = {
    val method = scala.util.Sorting.getClass.getDeclaredMethod(
      "sort1", classOf[Array[Int]]
      , classOf[Int]
      , classOf[Int]
    )
    method.setAccessible(true)
    (xs: Array[Int], offset: Int, len: Int) => {
      method.invoke(scala.util.Sorting, xs, offset.asInstanceOf[AnyRef], len.asInstanceOf[AnyRef])
    }
  }

  def quickSort(xs: Array[Int], offset: Int, length: Int): Unit = {
    //sort1(xs, offset, length)
    xs.slice(offset, offset + length).toSeq.sorted.copyToArray(xs, offset)
  }

  @volatile var dummy: AnyRef = null

  def parMergeSort(xs: Array[Int], maxDepth: Int): Unit = {
    val ys = new Array[Int](xs.length)
    dummy = ys

    def merge(src: Array[Int], dst: Array[Int], from: Int, mid: Int, until: Int) {
      var left = from
      var right = mid
      var i = from
      while (left < mid && right < until) {
        while (left < mid && src(left) <= src(right)) {
          dst(i) = src(left)
          i += 1
          left += 1
        }
        while (right < until && src(right) <= src(left)) {
          dst(i) = src(right)
          i += 1
          right += 1
        }
      }
      while (left < mid) {
        dst(i) = src(left)
        i += 1
        left += 1
      }
      while (right < mid) {
        dst(i) = src(right)
        i += 1
        right += 1
      }
    }

    def sort(from: Int, until: Int, depth: Int): Unit = {
      if (depth == maxDepth) {
        quickSort(xs, from, until - from)
      } else {
        val mid = (from + until) / 2
        val right = task {
          sort(mid, until, depth + 1)
        }
        sort(from, mid, depth + 1)
        right.join()

        val flip = (maxDepth - depth) % 2 == 0
        val src = if (flip) ys else xs
        val dst = if (flip) xs else ys
        merge(src, dst, from, mid, until)
      }
    }

    sort(0, xs.length, 0)

    def copy(src: Array[Int], target: Array[Int], from: Int, until: Int, depth: Int): Unit = {
      if (depth == maxDepth) {
        Array.copy(src, from, target, from, until - from)
      } else {
        val mid = (from + until) / 2
        val right = task {
          copy(src, target, mid, until, depth + 1)
        }
        copy(src, target, from, mid, depth + 1)
        right.join()
      }
    }

    copy(ys, xs, 0, xs.length, 0)
  }

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 20,
    Key.exec.maxWarmupRuns -> 60,
    Key.exec.benchRuns -> 60,
    Key.verbose -> true
  ) withWarmer (new Warmer.Default)

  //var xs=Random.shuffle((0 to 10).toArray).toArray
  val xs = (0 to 10).toArray
  xs foreach println
  println("")
  parMergeSort(xs, 3)
  xs foreach print
}


