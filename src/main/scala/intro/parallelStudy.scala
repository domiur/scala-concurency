package intro

import common.parallel

import org.scalameter._
import scala.util.Random


object parallelSum extends App{

  def sum(array: Array[Int])= {
    var s=0
    for(a <- array) s=s+a
    s
  }

  def sumSegment(array: Array[Int],from:Int,until:Int):Int={
    var s=0
    var i=from
    while(i<until) {
      s=s+array(i)
      i+=1
    }
    s
  }

  val ar=Array(1,2,3,4,5,6)
  println(sum(ar))
  println(sumSegment(ar,1,6))


  val (s1,s2)=parallel(sumSegment(ar,0,3),sumSegment(ar,3,6))
  println((s1+s2).toString)

  val threshold=3
  def sumRecursive(array: Array[Int],from:Int,until:Int):Int={
    if (until-from< threshold){
      sumSegment(array,from,until)
    }
    else{
      val t=(until-from)/2
      val (s1,s2)=parallel(sumSegment(array,from,from+t),sumSegment(array,from+t,until))
      s1+s2
    }
  }
  println(sumRecursive(ar,0,ar.length))
}


object MonteCarloForPI extends App{

  def countSeq(iter:Int):Int={
    val (rx,ry)=(new Random,new Random)
    var hits=0
    for(_ <- 0 until iter){
      val x=rx.nextDouble()
      val y=ry.nextDouble()
      if(x*x+y*y<1) hits+=1
    }
    hits
  }
  val threshold=1000
  def countPar(iter:Int):Int={
    if(iter<threshold){
      countSeq(iter)
    }
    else{
      val (h1,h2)=parallel(countPar(iter/2),countPar(iter-iter/2))
      h1+h2
    }
  }

  val iter=100000000
  //println(4.0*mccountPar(iter)/iter)
  println(4.0*countSeq(iter)/iter)
}




object timeMeasure extends App{
  val time = measure {
    (0 until 1000000).toArray
  }
  println(s"Array initialization time: $time ms")

  val time1 = withWarmer(new Warmer.Default) measure {
    (0 until 1000000).toArray
  }
  println(s"Array initialization time: $time1 ms")

  val time2 = config(
    Key.exec.minWarmupRuns -> 20,
    Key.exec.maxWarmupRuns -> 60,
    Key.verbose -> true
  ) withWarmer(new Warmer.Default) measure {
    (0 until 1000000).toArray
  }
  println(s"Array initialization time: $time2 ms")

  val time3 = withMeasurer(new Measurer.MemoryFootprint) measure {
    (0 until 1000000).toArray
  }
  println(s"Array initialization time: $time3 ms")

}





object parallelMapping extends App{
  def mapSeq[A,B](lst: List[A], f : A => B): List[B] = lst match {
    case Nil => Nil
    case h :: t => f(h) :: mapSeq(t,f)
  }
  println(mapSeq(List(1,2,3,4,5,6),(x:Int)=>x*x*x))


  def mapSegmentSeq[A,B](inp: Array[A], left: Int, right: Int, f : A => B, out: Array[B]) = {
    for (i <- left until right) {
      out(i) = f(inp(i))
    }
  }
  val in= Array(2,3,4,5,6)
  val out= Array(0,0,0,0,0)
  //val f= (x:Int) => {Thread.sleep(1);x*x*x}
  val f= (x:Int) => {x*x*x}

  mapSegmentSeq(in, 1, 3, f, out)
  println(out.toList)

  val threshold=1000
  def mapSegmentPar[A,B](inp: Array[A], left: Int, right: Int, f : A => B, out: Array[B]):Unit = {
    if( right-left < threshold)
      for (i <- left until right) {
        out(i) = f(inp(i))
      }
    else{
      val n=(left+right)/2
      parallel(mapSegmentPar(inp,left,n,f,out),mapSegmentPar(inp,n,right,f,out))
    }
  }
  mapSegmentPar(in, 0, in.length, f, out)
  println(out.toList)


  val conf = config(
    Key.exec.minWarmupRuns -> 20,
    Key.exec.maxWarmupRuns -> 60,
    Key.verbose -> true
  ) withWarmer(new Warmer.Default)

  val arin=new Array[Int](10000)
  val arout=new Array[Int](arin.length)

  val time1=conf measure {
    for (i <- arin.indices) {
      arin(i) = Random.nextInt()
    }
    mapSegmentPar(arin,0,arin.length,f,arout)
  }
  val time2=conf measure {
    for (i <- arin.indices) {
      arin(i) = Random.nextInt()
    }
    mapSegmentSeq(arin,0,arin.length,f,arout)
  }

  println(s"$time1 $time2")


  sealed abstract class Tree[A] { val size: Int }
  case class Leaf[A](a: Array[A]) extends Tree[A] {
    override val size = a.length
  }
  case class Node[A](l: Tree[A], r: Tree[A]) extends Tree[A] {
    override val size = l.size + r.size
  }
  def addTree(n:Int,size:Int):Tree[Int]={
    if(n==0){
      val ar = new Array[Int](size);
      for (i <- ar.indices) {
        ar(i) = Random.nextInt()
      }
      Leaf(ar)
    }
    else{
      Node(addTree(n-1,size),addTree(n-1,size))
    }
  }
  def mapTreePar[A:Manifest,B:Manifest](t:Tree[A],f: A => B):Tree[B]=t match {
    case Leaf(a:Array[A]) => {
      val b = new Array[B](a.length)
      for (i <- a.indices) b(i) = f(a(i))
      Leaf(b)
    }
    case Node(l, r) => val (a,b)=parallel(mapTreePar(l,f),mapTreePar(r,f))
      Node(a,b)
  }
  def mapTreeSeq[A:Manifest,B:Manifest](t:Tree[A],f: A => B):Tree[B]=t match {
    case Leaf(a:Array[A]) => {
      val b = new Array[B](a.length)
      for (i <- a.indices) b(i) = f(a(i))
      Leaf(b)
    }
    case Node(l, r) => val (a,b)=(mapTreeSeq(l,f),mapTreeSeq(r,f))
      Node(a,b)
  }

  val time3=conf measure {
    val t=addTree(11,10000)
    mapTreeSeq(t,f)
  }
  val time4=conf measure {
    val t=addTree(11,10000)
    mapTreePar(t,f)
  }

  println(s"$time1 $time2 $time3 $time4")

}




object parallelOperation extends App{
  abstract class Tree[A]
  case class Leaf[A](v:A) extends Tree[A]
  case class Node[A](l:Tree[A],r:Tree[A]) extends Tree[A]

  def reduce[A](t:Tree[A],f:(A,A)=>A):A =
    t match {
      case Leaf(v) => v
      case Node(l,r) => f(reduce(l,f),reduce(r,f))
    }
  def reducePar[A](t:Tree[A],f:(A,A)=>A):A =
    t match {
      case Leaf(v) => v
      case Node(l,r) => val(vl,vr)=parallel(reduce(l,f),reduce(r,f))
        f(vl,vr)
    }

  val t1=Node[Int]( Node(Leaf(1),Leaf(2)),Leaf(3))
  val t2=Node[Int]( Leaf(3),Node(Leaf(1),Leaf(2)))
  println(reduce(t1,(x:Int,y:Int)=>x+y))
  println(reduce(t2,(x:Int,y:Int)=>x+y))
  println(reduce(t1,(x:Int,y:Int)=>x-y))
  println(reduce(t2,(x:Int,y:Int)=>x-y))

  println(reducePar(t1,(x:Int,y:Int)=>x+y))
  println(reducePar(t2,(x:Int,y:Int)=>x+y))
  println(reducePar(t1,(x:Int,y:Int)=>x-y))
  println(reducePar(t2,(x:Int,y:Int)=>x-y))


  def toList[A](t: Tree[A]): List[A] = t match {
    case Leaf(v) => List(v)
    case Node(l, r) => toList[A](l) ++ toList[A](r) }
  def map[A,B](t: Tree[A], f : A => B): Tree[B] = t match {
    case Leaf(v) => Leaf(f(v))
    case Node(l, r) => Node(map[A,B](l, f), map[A,B](r, f)) }

  println(toList(t1))
  println(map[Int,List[Int]](t1,List(_)))
  println(reduce[List[Int]](map(t1, (x:Int) => List(x)), _ ++ _) )

  val threshold=1000
  def reduceSeg[A](inp: Array[A], left: Int, right: Int, f: (A,A) => A): A = {
    if (right - left < threshold) {
      var res= inp(left)
      for(i <- left+1 until right) res= f(res, inp(i))
      res
    } else {
      val mid = (right + left)/2
      val (a1,a2) = parallel( reduceSeg(inp, left, mid, f),  reduceSeg(inp, mid, right, f) )
      f(a1,a2)
    }
  }
  def reduceArr[A](inp: Array[A], f: (A,A) => A): A =
    reduceSeg(inp, 0, inp.length, f)

  val conf = config(
    Key.exec.minWarmupRuns -> 20,
    Key.exec.maxWarmupRuns -> 60,
    Key.verbose -> true
  ) withWarmer(new Warmer.Default)

  val ar=new Array[Int](10000)
  val time1=conf measure{
    for(i <- ar.indices) ar(i)=i
    reduceArr(ar,(x:Int,y:Int)=>x+y)
  }
  println(s"$time1")
}







object Exercise51 extends App{



  def xcombinations[A](l:List[A],n: Int): List[List[A]] =
    if (n > l.size) Nil
    else l match {
      case _ :: _ if n == 1 =>  l.map(List(_))
      case hd :: tl =>  xcombinations(tl,n - 1).map(hd :: _) ++ xcombinations(tl,n)
      case _ => Nil
    }
  print(xcombinations("4567".toList.map(_.toString),3).map(_.mkString("")));println()

  def xsubsets[A](l:List[A]): List[List[A]] =
    for( i <- (1 to l.size).toList;a <- xcombinations(l,i) ) yield a
  //(2 to l.size).foldLeft(xcombinations(l,1))((a, i) => xcombinations(l,i) ::: a)

  print(xsubsets("4567".toList.map(_.toString)).map(_.mkString("")));println()

  def xvariations[A](l:List[A],n: Int): List[List[A]] = {
    def mixmany(x: A, ll: List[List[A]]): List[List[A]] = ll match {
      case hd :: tl => foldone(x, hd) ::: mixmany(x, tl)
      case _ => Nil
    }
    def foldone(x: A, ll: List[A]): List[List[A]] =
      (1 to ll.length).foldLeft(List(x :: ll))((a, i) => (mixone(i, x, ll)) :: a)
    def mixone(i: Int, x: A, ll: List[A]): List[A] =
      ll.slice(0, i) ::: (x :: ll.slice(i, ll.length))

    if (n > l.size) Nil
    else l match {
      case _ :: _ if n == 1 => l.map(List(_))
      case hd :: tl => mixmany(hd, xvariations(tl,n - 1)) ++ xvariations(tl,n)
      case _ => Nil
    }
  }
  print(xvariations("456".toList.map(_.toString),3).map(_.mkString("")));println()






  def repeatingPerm[T](elems: List[T], genSize: Int): List[List[T]] = {
    def repeatingPermRec(elems: List[T], depth: Int, partResult: List[T]): List[List[T]]  = depth match {
      case 0 => List()
      case 1 => for (elem <- elems)  yield elem :: partResult
      case n => for (elem <- elems;l <- repeatingPermRec(elems, n - 1, elem :: partResult)) yield l
    }
    if (genSize < 0) throw new IllegalArgumentException("Negative lengths not allowed in repeatingPerm...")
    repeatingPermRec(elems, genSize, Nil)
  }

  print(repeatingPerm("132".toList.map(_.toString),2).map(_.mkString("")));println()

  def nonRepeatingPerm[T](elems: List[T], genSize: Int): List[List[T]]  = {
    def removeElem(elems: List[T], elem: T): List[T] = elems match {
      case Nil => Nil
      case head :: tail => if (head == elem) removeElem(tail, elem) else head :: removeElem(tail, elem)
    }
    def nonRepeatingPermRec(elems: List[T], depth: Int, partResult: List[T]): List[List[T]]  = depth match {
      case 0 => List()
      case 1 => for (elem <- elems) yield elem :: partResult
      case n => for (elem <- elems;l <- nonRepeatingPermRec(removeElem(elems, elem), n - 1, elem :: partResult)) yield l
    }
    if (genSize < 0) throw new IllegalArgumentException("Negative generation sizes not allowed in nonRepeatingPerm...")
    if (genSize > elems.size) throw new IllegalArgumentException("Generation sizes over elems.size not allowed in nonRepeatingPerm...")
    nonRepeatingPermRec(elems, genSize,  Nil)
  }
  print(nonRepeatingPerm("123".toList.map(_.toString),2).map(_.mkString("")));println()

  def nonRepeatingComb[T](elems: List[T], genSize: Int): List[List[T]] = {
    def nonRepeatingCombRec(elems: List[T], depth: Int, partResult: List[T]): List[List[T]]  = {
      if (depth<0) List(Nil)
      if (elems.size == depth) List(elems.reverse ::: partResult)
      else {
        if (!elems.isEmpty) {
          nonRepeatingCombRec(elems.tail, depth, partResult)++ nonRepeatingCombRec(elems.tail, depth - 1, elems.head :: partResult)
        }
        else
          List(Nil)
      }
    }
    if (genSize < 0) throw new IllegalArgumentException("Negative generation sizes not allowed in nonRepeatingComb...")
    if (genSize > elems.size) throw new IllegalArgumentException("Generation sizes over elems.size not allowed in nonRepeatingComb...")
    nonRepeatingCombRec(elems, genSize, Nil)
  }
  print(nonRepeatingComb("123".toList.map(_.toString),2).map(_.mkString("")));println()

  def repeatingComb[T](elems: List[T], genSize: Int, f: List[T] => Unit): Unit = {
    val elemsReverse = elems.reverse
    def folder(triple: (List[T], Int, List[T]), index: Int): (List[T], Int, List[T]) = triple match {
      case (elems, i, result) => (elems.drop(index - i), index + 1, elems.drop(index - i).head :: result)
    }
    def mapSimulation(input: List[Int]): List[T] = (((elemsReverse, 0, Nil: List[T]) /: input) (folder))._3
    def nonRepeatCombRec(elems: List[Int], depth: Int, partResult: List[Int]): Unit = {
      if (elems.size == depth) f(mapSimulation(partResult.reverse ::: elems))
      else {
        if (!elems.isEmpty) {
          nonRepeatCombRec(elems.tail, depth, partResult)
          if (depth > 0) nonRepeatCombRec(elems.tail, depth - 1, elems.head :: partResult)
        }
      }
    }
    if (genSize < 0) throw new IllegalArgumentException("Negative generation sizes not allowed in repeatingComb...")
    val simulationSize = genSize + elems.length - 1
    nonRepeatCombRec((0 until simulationSize).toList, genSize, Nil)
  }

  repeatingComb("123".toList.map(_.toString),3,print);println()

}



