name := "scala-concurency"

version := "0.1"

scalaVersion := "2.13.8"
libraryDependencies += "org.scalameta" %% "munit" % "0.7.29" % Test
libraryDependencies +="com.storm-enroute" %% "scalameter-core" % "0.19"

